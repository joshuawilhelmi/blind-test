const fs = require('fs')
const _ = require('lodash')


const findStaticDataFiles = function() {
    const files = fs.readdirSync(__dirname)
    return files.filter(f => _.endsWith(f, '.data.json')) 
}



const update = function (app) {
    const files = findStaticDataFiles()
    let dataToUpdate
    let model
    let propertyToSearch

    return files.map(function(f) {
            return new Promise((resolve, reject) => {
                console.log('Mise à jour des données ', f)
                dataToUpdate = require('./' + f)
                model = require('../../src/db/' + f.split('.')[0] + '.model')
                dataToUpdate.forEach(o => {
                    console.log('Mise à jour de l\'objet', o)
                    for (const property in o) {
                        if (o.hasOwnProperty(property)) {
                            propertyToSearch = property
                            break;
                        }
                    }
                    let objToSearch = {}
                    objToSearch[propertyToSearch] = o[propertyToSearch]
                    model.findOneAndUpdate(objToSearch, o, {upsert: true, new: true, setDefaultsOnInsert: true}, function (err, raw) {
                        if (err) {
                            console.error(err)
                            reject()
                        }
                        else {
                            console.log('The raw response from Mongo was ', raw);
                            resolve()
                        }
                    })
                })
            })
    })
}

const db = require('../../src/db/db')
db.initConnection()
db.connection.once('open', function() {
  console.log('Database connected')
  Promise.all(update()).then(() => {
    db.connection.close()
  }).catch(console.error)
});
