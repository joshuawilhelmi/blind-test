const fs = require('fs')
const _ = require('lodash')
const execSync = require('child_process').execSync;

let i = 1;

function parseMusicInfos(f) {
    let musicInfos = {
        index: i++,
        filename: f
    }
    const fileName = f.substring(0, f.length - 4)
    if (fileName.includes('__')) {
        [ musicInfos.oeuvre, musicInfos.title ] = fileName.split('__')
        musicInfos.title = musicInfos.title.split('_').join(' ')
    } else {
        musicInfos.oeuvre = fileName
    }
    musicInfos.oeuvre = musicInfos.oeuvre.split('_').join(' ')
    return musicInfos
}


const rootDir = process.argv[2] || __dirname + '/../../musiques/'
if (!fs.lstatSync(rootDir).isDirectory()) {
    throw new Error(`${rootDir} n'est pas un répertoire ou n'est pas accessible`)
} else {
    console.log(`${rootDir} est accessible`);
    const files = fs.readdirSync(rootDir)
        .filter(f => _.endsWith(f, '.mp3'))
        .map(f => {
            execSync(`sox "${rootDir}${f}" "${__dirname}/../../musiques/${f}" trim 0 00:30`, (err, stdout, stderr) => {
                if (err) {
                  throw new Error(err)
                }
            });
            return parseMusicInfos(f)
        })

    fs.writeFileSync(__dirname + '/../static/musique.data.json', JSON.stringify(files, null, 2))
}
