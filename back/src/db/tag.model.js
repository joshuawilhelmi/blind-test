const mongoose = require('mongoose')

const TagSchema = new mongoose.Schema(
        {
                libelle: {
                        type: String,
                        require: true,
                        index: true
                },
                poids: {
                        type: Number
                }
        }
);

module.exports = mongoose.model('Tag', TagSchema);
