const mongoose = require('mongoose')
const random = require('mongoose-random')

// build user schema
const MusiqueSchema = new mongoose.Schema(
        {
                id: {
                        type: Number,
                        require: true,
                        index: true,
                        unique: true
                },
                oeuvre: {
                        type: String,
                        require: true
                },
                titre: {
                        type: String,
                        require: true,
                },
                auteur: {
                        type: String
                },
                fichier: {
                        type: String,
                        require: true
                },
                tags:  [{
                        type: String
                }]
        }
)

MusiqueSchema.plugin(random, { path: 'r' });

module.exports = mongoose.model('Musique', MusiqueSchema); // export model for use
