const db = {}

db.initConnection = function() {
    const mongoose = require('mongoose')
    mongoose.connect('mongodb://localhost/blindtest')
    db.connection = mongoose.connection
    db.connection.on('error', console.error.bind(console, 'connection error:'))    
}

module.exports = db