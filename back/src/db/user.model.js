const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema(
        {
                login: {
                        type: String,
                        require: true,
                        index: true
                },
                password: {
                        type: String,
                        require: true,
                        bcrypt: true
                },
                isAdmin:  {
                        type: Boolean
                },
                scoreSolo: {
                        type: Number
                }
        }
);





UserSchema.plugin(require('mongoose-bcrypt'));


module.exports = mongoose.model('User', UserSchema);