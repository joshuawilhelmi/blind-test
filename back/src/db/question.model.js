const mongoose = require('mongoose')

const QuestionSchema = new mongoose.Schema(
        {
                musique: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Musique',
                        require: true,
                        index: true
                },
                propositions: {
                        type: Array
                },
                reponses:  {
                        type: Array
                },
                token: {
                    type: String
                }
        }
);

module.exports = mongoose.model('Musique', MusiqueSchema);
