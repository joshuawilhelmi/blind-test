const auth = require('./lib/auth')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const routes = require('./api/routes.js')
const app = require('express')()
const formidable = require('formidable');

const server = require('http').createServer(app);
const io = require('socket.io')(server)

app.use(bodyParser.json())
app.use(methodOverride())
app.use(function (req, res, next) {
  if (req.method === 'OPTIONS' || req.url !== '/admin/music') {
    next()
  } else {
    const form = new formidable.IncomingForm({
      encoding: 'utf-8',
      uploadDir: 'files',
      multiples: false,
      keepExtensions: true
    })
    form.once('error', console.log)
    form.parse(req, function (err, fields, files) {
      Object.assign(req, { fields, files });
      next();
    })
  }
})
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization")
  res.header("Access-Control-Allow-Credentials", "true")
  if ('OPTIONS' === req.method) {
    console.log('PREFLIGHT recieved => OK')
    res.send(200);
  } else {
    next();
  }
});

app.all('*', auth.check)
routes.assignRoutes(app)

io.on('connection', function (socket) {
  console.log('user connected')

  socket.on('ding', function () {
    socket.emit('dong')
  })

  socket.on('disconnect', function () {
    console.log('user disconnected')
  })
})

const db = require('./db/db')
db.initConnection()
db.connection.once('open', function () {
  console.log('Database connected')
  server.listen(3000)
  console.log('Server listening on port 3000')
});