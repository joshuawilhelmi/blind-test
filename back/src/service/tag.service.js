const TagModel = require('../db/tag.model')

exports.findTags = function() {
    return new Promise((resolve, reject) => {
        TagModel.find({}, 'libelle poids', function (err, tags) {
            if (err) {
                reject(err)
            }
            resolve(tags)
        })
    })
}