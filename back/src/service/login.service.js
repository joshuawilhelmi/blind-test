const auth = require('../lib/auth')
const UserModel = require('../db/user.model')
const bcrypt = require('bcrypt')

exports.login = function (form) {
    return new Promise((resolve, reject) => {
        if (!form.login || !form.password) {
            reject({
                status: 400,
                message: 'Un login et un mot de passe sont nécessaires pour se connecter'
            })
        } else {
            UserModel.findOne({login: form.login}, function(err, user) {
                if (err || !user) {
                    reject({
                        status: 401,
                        message: "Erreur d'authentification"
                    })
                } else {
                    bcrypt.compare(form.password, user.password, function(err, isMatch) {
                        if (err || !isMatch) {
                            reject({
                                status: 401,
                                message: "Erreur d'authentification"
                            })
                        } else {
                            resolve({
                                isAdmin: user.isAdmin,
                                token: auth.createToken(user)
                            })
                        }
                    })
                }
            })
        }
    })
}

exports.createUser = function (user) {
    return new Promise((resolve, reject) => {
        if (!user.login || !user.password) {
            reject({
                status: 400,
                message: 'Un login et un mot de passe sont nécessaires pour créer un utilisateur'
            })
        } else {
            if (!user.isAdmin) {
                user.isAdmin = false
            }
            UserModel.create(user, function(err, u) {
                if (err) {
                    console.error(err)
                    reject({
                        status: 500,
                        message: err
                    })
                } else {
                    resolve(u)
                }
            })
        }

    })
}

exports.findUsers = function() {
    return new Promise((resolve, reject) => {
        UserModel.find({}, function(err, users) {
            if (!err){ 
                resolve(users)
            } else {
                reject({message: err})
            }
        })
    })
}