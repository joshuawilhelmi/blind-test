const MusiqueModel = require('../db/musique.model')
const redisClient = require('../lib/redis-client')
const uniqid = require('uniqid')

exports.generateQuestions = function (nb) {
    return new Promise((resolve, reject) => {
        MusiqueModel.findRandom({}, 'id').limit(nb).exec(function (err, songs) {
            if (err) {
                reject(err)
            }
            const tokens = []
            let token
            songs.forEach(
                song => {
                    token = uniqid()
                    console.log('Insertion REDIS : ', {key: token, value: song.id})
                    redisClient.set(token, song.id, 'EX', 3600)
                    tokens.push(token)
                }
            )
            resolve(tokens)
        });
    })
}

function generationQuestion(id) {
    let propositions = []
    return new Promise((resolve, reject) => {
        MusiqueModel.findOne({ 'id': id }).exec(function (err, song) {
            if (err) {
                reject(err)
            }
            propositions.push(song)
            console.log('Premiere proposition => ', propositions)
            const tags = song.tags
            MusiqueModel.findRandom({ tags: tags, id: { $ne: song.id } }, 'oeuvre tags').limit(3).exec(function (err, songs) {
                if (err) {
                    reject(err)
                }
                console.log('Autres propositions avec le même tag => ', songs)
                propositions = [...propositions, ...songs]
                shuffle(propositions)
                console.log('Propositions finales => ', propositions)
                const question = {
                    id: song.id,
                    tags: song.tags,
                    propositions: propositions.map(p => p.oeuvre)
                }
                resolve(question)
            })
        })
    })
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

exports.generateQuestionById = function (id) {
    return generateQuestion(id)
}

exports.generateQuestionByToken = function (token) {
    console.log('Token  : ', token)
    const promise = new Promise((resolve, reject) => {
        redisClient.get(token, function(err, reply) {
            console.log('Récupération REDIS : ', {key: token, value: token})
            if (err || reply == null) {
                reject('La question est expirée')
            } else {
                generationQuestion(reply).then(
                    res => {
                        resolve(res)
                    },
                    error => {
                        reject(error)
                    }
                )
            }
        })
    })
    return promise
}

exports.findMusicFileById = function (id) {
    return new Promise((resolve, reject) => {
        MusiqueModel.findOne({ 'id': id }).exec(function (err, song) {
            if (err) {
                reject(err)
            }
            resolve(song.fichier)
        })
    })
}

exports.checkAnswer = function (id, answer) {
    return new Promise((resolve, reject) => {
        MusiqueModel.findOne({ 'id': id }).exec(function (err, song) {
            if (err) {
                reject(err)
            }
            console.log('Answer recieved : ', answer)
            console.log('Correct answer : ', song.oeuvre)
            resolve({
                success: song.oeuvre === answer
            })
            resolve(song.fichier)
        })
    })
}