const blindtestService = require('../service/blindtest.service')
const fs = require('fs')

function generateQuestions(req, res, next) {
    const nb = parseInt(req.params.nb)
    blindtestService.generateQuestions(nb).then(
        result => {
            res.send(result)
        },
        error => {
            res.status(500).send(error)
        }
    )
    
}

function generateQuestionById(req, res, next) {
    const id = parseInt(req.params.id)
    blindtestService.generateQuestionById(id).then(
        result => {
            res.send(result)
        },
        error => {
            res.status(500).send(error)
        }
    )
}

function generateQuestionByToken(req, res, next) {
    blindtestService.generateQuestionByToken(req.params.token).then(
        result => {
            res.send(result)
        },
        error => {
            res.status(500).send(error)
        }
    )
}

function findMusic(req, res, next) {
    const id = req.params.id
    res.set('Content-Type', 'audio/mp3');
    res.set('accept-ranges', 'bytes');
    blindtestService.findMusicFileById(id).then(
        musicFile => {
            const readStream = fs.createReadStream('musiques/' + musicFile)
            readStream.on('open', function () {
                readStream.pipe(res)
            })
            readStream.on('end', function () {
                res.end()
            })
        },
        error => {
            res.status(500).send(error)
        }
    )
}

function answerQuestion(req, res, next) {
    const id = req.params.id
    blindtestService.checkAnswer(id, req.body.answer).then(
        result => {
            res.send(result)
        },
        error => {
            res.status(500).send(error)
        }
    )
}

function newGame(req, res, next) {
    blindtestService.generateQuestions(10).then(
        questions => {
            res.send(questions)
        },
        error => {
            res.status(500).send(error)
        }
    )
}

exports.initRoutes = function (app) {
    app.get('/questions/generate/:nb', generateQuestions)
    app.get('/question/:id', generateQuestionById)
    app.get('/public/music/:id', findMusic)
    app.post('/blindtest/question/answer/:id', answerQuestion)
    app.get('/blindtest/new', newGame)
    app.get('/blindtest/question/:token', generateQuestionByToken)
}
