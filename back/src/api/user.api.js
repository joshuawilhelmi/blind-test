
const loginService = require('../service/login.service')

const login = function(req, res) {
    loginService.login(req.body).then(
        result => {
            res.json({message: 'Login Success', token: result.token, isAdmin: result.isAdmin})
        }, error => {
            res.status(error.status || 401).json({error: error.message})
        }     
    )
}

const getUser = function(req, res) {
    res.json({user: req.user})
}

const getUsers = function(req, res) {
    loginService.findUsers().then(
        users => {
            res.json(users)
        }, error => {
            res.status(error.status || 500).json({error: error.message})
        }
    )
}

const createUser = function(req, res) {
    loginService.createUser(req.body.user).then(
        result => {
            res.json({result: 'Success'})
        }, error => {
            res.status(error.status || 401).json({error: error.message})
        }
    )
}

exports.initRoutes = function(app) {
    app.post('/public/login', login)
    app.get('/user', getUser)
    app.post('/public/user', createUser)
    app.get('/public/users', getUsers)
}