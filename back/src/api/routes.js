const fs = require('fs')
const _ = require('lodash')

exports.assignRoutes = function (app) {
    let files = fs.readdirSync(__dirname)
    files = files.filter(f => _.endsWith(f, '.api.js'))
    files.forEach(f => {
        require('./' + f).initRoutes(app)
    })
}