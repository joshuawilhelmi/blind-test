const fs = require('fs')
const Music = require('../db/musique.model')

const musicByToken = function (req, res, next) {
    const token = req.params.token
    res.set('Content-Type', 'audio/mp3');
    res.set('accept-ranges', 'bytes');
    const readStream = fs.createReadStream('musiques/1.mp3')
    readStream.on('open', function () {
        readStream.pipe(res)
    })
    readStream.on('end', function () {
        res.end()
    })
}

const addMusic = function (req, res) {
    Music.create({
        titre: req.params.music,
        fichier: 'toto.mp3',
        genres: ['Film', 'Manga']
    }, function(err, result) {
        if (!err){ 
            res.send(JSON.stringify(result))
        } else {throw err;}
    })
}

const getMusic = function (req, res) {
    Music.find({}, function(err, musics) {
        if (!err){ 
            res.send(JSON.stringify(musics))
        } else {throw err;}
    })
}

exports.initRoutes = function (app) {
    app.get('/public/music/byToken/:token', musicByToken)
    app.get('/public/music/add/:music', addMusic)
    app.get('/public/music/get', getMusic)
}
