const adminService = require('../service/admin.service')

function addMusic(req, res, next) {
  adminService.addMusic(req.files.file).then(
    () => res.send('Files OK')
  )
}

function findMusics(req, res, next) {
  res.send('OK')
}





exports.initRoutes = function (app) {
  app.post('/admin/music', addMusic)
  app.get('/admin/musics', findMusics)
}
