const tagService = require('../service/tag.service')

const findTags = function(req, res, next) {
    tagService.findTags().then(
        result => {
            res.send(result)
        }, error => {
            res.status(500).send(error)
        }
    )
}

exports.initRoutes = function(app) {
    app.get('/tags', findTags)
}