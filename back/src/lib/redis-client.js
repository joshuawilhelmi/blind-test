const redisClient = require('redis').createClient()

redisClient.on("error", function (err) {
    console.error("Redis client Error " + err)
})

module.exports = redisClient