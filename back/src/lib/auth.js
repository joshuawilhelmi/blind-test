const jwt = require('jsonwebtoken')
const _ = require('lodash')

const secretjwt = 'secretjwt'

const verifyJwtToken = function (token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secretjwt, (err, decodedToken) => {
            if (err || !decodedToken) {
                return reject(err)
            }
            resolve(decodedToken)
        })
    })
}


exports.check = function (request, response, next) {
    if (_.startsWith(request.url, '/public/')) {
        next()
    } else {
        // console.log('Method recieved : ', request.method)
        console.log('Headers recieved : ', request.headers)
        let token = request.headers.authorization
        if (token) {
            token = token.replace('Bearer ', '').trim()
            verifyJwtToken(token)
                .then((decodedToken) => {
                    request.user = decodedToken.data
                    next()
                })
                .catch(() => {
                    response.status(400)
                        .json({
                            error: "Token invalide"
                        })
                })
        } else {
            response.status(400)
            .json({
                error: "Token manquant"
            })
        }
        
    }
}

exports.createToken = function (sessionData) {
    let token = jwt.sign({
        data: sessionData
    }, secretjwt, {
            expiresIn: 3600,
            algorithm: 'HS256'
        })

    return token
}