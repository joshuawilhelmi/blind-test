const chai = require("chai")
const chaiAsPromised = require("chai-as-promised")

chai.use(chaiAsPromised)
chai.should()

const loginService = require('../../src/service/login.service')

describe('Login service', function() {
    it('Login admin shoudl be OK', function() {
        return loginService.login({login: 'admin', password: 'admin'}).should.be.ok
    })
    it('Login admni should be KO', function() {
        return loginService.login({login: 'admni', password: 'admin'}).should.be.rejected
    })
    it('Login with missing params should be KO', function() {
        return loginService.login({login: 'admin'}).should.be.rejected
    })
})
