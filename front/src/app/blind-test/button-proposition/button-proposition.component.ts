import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'button-proposition',
  templateUrl: './button-proposition.component.html',
  styleUrls: ['./button-proposition.component.css']
})
export class ButtonPropositionComponent implements OnInit {

  @Input()
  text;
  
  @Output()
  propositionClicked = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() {
  }

  buttonClicked() {
    this.propositionClicked.emit(true);
  }

}
