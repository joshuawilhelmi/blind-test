import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonPropositionComponent } from './button-proposition.component';

describe('ButtonPropositionComponent', () => {
  let component: ButtonPropositionComponent;
  let fixture: ComponentFixture<ButtonPropositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonPropositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonPropositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
