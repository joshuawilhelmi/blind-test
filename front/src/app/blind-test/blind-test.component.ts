import { Component, OnInit, NgZone } from '@angular/core';
import { ChatService } from './chat.service';
import { BlindTestService } from './blind-test.service';
import { environment } from '../../environments/environment';
import { Howl } from 'howler';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-blind-test',
  templateUrl: './blind-test.component.html',
  styleUrls: ['./blind-test.component.css']
})
export class BlindTestComponent implements OnInit {
  currentQuestionId;
  currentQuestion;
  currentPropositions;
  nbSecondsToWait = 3;
  player;
  isLoading = true;
  songLoaderEvent = new BehaviorSubject('');
  progressBarValue = 100;
  numQuestion = 0;

  private timerWaitQuestion;
  private timerProgressBar;

  constructor(private zone: NgZone, private blindTestService: BlindTestService, private router: Router) { }

  ngOnInit() {
    this.songLoaderEvent.subscribe(
      onSongLoaded => {
        if (onSongLoaded === 'Song loaded') {
          console.log('Event loaded reçu, lancement du timer')
          this.launchTimer()
        }
      }
    );
    this.loadNextQuestion();
  }

  answer(index: number) {
    clearTimeout(this.timerProgressBar);
    this.isLoading = true;
    console.log('Réponse : ', this.currentPropositions[index]);
    this.blindTestService.answerQuestion(this.currentQuestionId, this.currentPropositions[index]).subscribe(
      result => {
        console.log('Résultat : ', result);
        this.player.stop();
        if (result.success) {
          this.blindTestService.score++;
        }
        this.loadNextQuestion();
      }
    );
  }

  private launchTimer() {
    console.log('Launch timer !');
    this.numQuestion++;
    this.nbSecondsToWait = 3;
    this.progressBarValue = 100;
    this.isLoading = false;
    this.timerWaitQuestion = setInterval(
      () => {
        if (this.nbSecondsToWait === 0) {
          this.player.play();
          clearInterval(this.timerWaitQuestion);
          this.timerProgressBar = setInterval(() => {
            this.progressBarValue = Math.max(0, this.progressBarValue - 0.333);
            if (this.progressBarValue === 0) {
              this.endQuestion();
            }
            this.zone.run(() => { });
          }, 100);
        } else if (this.nbSecondsToWait > 0) {
          this.nbSecondsToWait--;
        }
        this.zone.run(() => { });
      }, 1000
    );
  }

  private loadNextQuestion() {
    this.blindTestService.loadNextQuestion().subscribe(
      (result: any) => {
        this.currentQuestionId = result.id;
        this.currentQuestion = '[' + result.tags.join(', ') + '] Quelle oeuvre contient cette musique ?';
        this.currentPropositions = result.propositions;
        this.player = new Howl({
          src: [environment.backUrl + 'public/music/' + result.id],
          format: 'mp3'
        });
        this.player.once('load', () => {
          console.log('Song loaded');
          this.songLoaderEvent.next('Song loaded');
        });
      },
      error => {
        this.endGame();
      });
  }

  private endQuestion() {
    clearTimeout(this.timerProgressBar);
    this.player.stop();
    this.isLoading = true;
    this.loadNextQuestion();
  }

  private endGame() {
    this.router.navigate(['/gameover']);
  }


}
