import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlindTestComponent } from './blind-test.component';

describe('BlindTestComponent', () => {
  let component: BlindTestComponent;
  let fixture: ComponentFixture<BlindTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlindTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlindTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
