import { Injectable, OnInit } from '@angular/core';
import * as socketIo from 'socket.io-client';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatService implements OnInit {

  public ping = -1;
  private socket;
  private lastPingTime;

  constructor() { }

  ngOnInit() {
  }

  connect() {
    this.socket = socketIo(environment.backUrl);
    this.socket.on('connect', () => {
      this.socket.on('dong', () => {
        this.ping = new Date().getTime() - this.lastPingTime;
      });
      setInterval(() => {
        this.lastPingTime = new Date().getTime();
        this.socket.emit('ding');
      }, 1000);
    });
  }
}
