import { Component, OnInit } from '@angular/core';
import { BlindTestService } from '../blind-test.service';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.css']
})
export class GameOverComponent implements OnInit {

  constructor(protected blindTestService: BlindTestService) { }

  ngOnInit() {
  }

}
