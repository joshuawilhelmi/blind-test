import { TestBed, inject } from '@angular/core/testing';

import { BlindTestService } from './blind-test.service';

describe('BlindTestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlindTestService]
    });
  });

  it('should be created', inject([BlindTestService], (service: BlindTestService) => {
    expect(service).toBeTruthy();
  }));
});
