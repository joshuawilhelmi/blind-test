import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class BlindTestService {
  score = 0;
  private currentGameQuestions;

  constructor(private http: HttpClient) { }

  initNewGame(): any {
    return new Observable((observer) => {
      this.http.get(environment.backUrl + 'blindtest/new').subscribe(
        res => {
          this.currentGameQuestions = res;
          observer.next();
        }, err => {
          observer.error(err);
        }
      )
    });
  }

  loadNextQuestion() {
    return new Observable((observer) => {
      if (!this.currentGameQuestions || this.currentGameQuestions.length === 0) {
        observer.error('No question');
      } else {
        const token = this.currentGameQuestions[0];
        this.currentGameQuestions.shift();
        this.http.get(environment.backUrl + 'blindtest/question/' + token).subscribe(
          res => {
            observer.next(res);
          }, err => {
            observer.error(err);
          }
        )
      }
      
    });
  }

  answerQuestion(questionId: number, answer: string): Observable<any> {
    return this.http.post(environment.backUrl + 'blindtest/question/answer/' + questionId, {answer: answer});
  }

}
