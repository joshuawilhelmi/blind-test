import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { BlindTestComponent } from './blind-test/blind-test.component';
import { LoginService } from './user/login.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CredentialsInterceptor } from './common/credentials.interceptor';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { AdminComponent } from './admin/admin.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatProgressBarModule } from '@angular/material';
import { GameOverComponent } from './blind-test/game-over/game-over.component';
import { ButtonPropositionComponent } from './blind-test/button-proposition/button-proposition.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'gameover', component: GameOverComponent },
  { path: 'login/comingFrom/:comingFrom', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [LoginService] },
  { path: 'blindtest', component: BlindTestComponent, canActivate: [LoginService] },
  { path: 'admin', component: AdminComponent, canActivate: [LoginService] }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BlindTestComponent,
    HeaderComponent,
    HomeComponent,
    AdminComponent,
    GameOverComponent,
    ButtonPropositionComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule
  ],
  providers: [
    LoginService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CredentialsInterceptor,
      multi: true,
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
