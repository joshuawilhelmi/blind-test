import { Component, OnInit, ViewChild } from '@angular/core';
import { FileUploadService } from './file-upload.service';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  @ViewChild('file') file;
  public files: Set<File> = new Set();
  progress;
  canBeClosed = true;
  primaryButtonText = 'Upload';
  showCancelButton = true;
  uploading = false;
  uploadSuccessful = false;

  constructor(private uploadService: FileUploadService) { }

  ngOnInit() {

  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
    // set the component state to "uploading"
    this.uploading = true;

    // start the upload and save the progress map
    const upload = this.uploadService.upload(this.files);
    for (const k in upload) {
      if (upload.hasOwnProperty(k)) {
        this.progress = upload[k].progress;
        this.progress.subscribe(() => this.files = new Set());
      }
    }


    // convert the progress map into an array
    const allProgressObservables = [];
    for (const key in this.progress) {
      if (this.progress.hasOwnProperty(key)) {
        allProgressObservables.push(this.progress[key].progress);
      }
    }
  }

  addFiles() {
    this.file.nativeElement.click();
  }
}
