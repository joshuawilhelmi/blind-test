import { Component, OnInit } from '@angular/core';
import { BlindTestService } from '../blind-test/blind-test.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private blindTestService: BlindTestService, private router: Router) { }

  ngOnInit() {
  }

  launchBlindTest() {
     this.blindTestService.initNewGame().subscribe(
        result => {
          console.log('Game Start');
          this.router.navigate(['/blindtest']);
        }, error => {
          console.error(error);
        }
     )
  }

}
