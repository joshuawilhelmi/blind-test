import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService {

  private user;

  constructor(private http: HttpClient, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot,): Observable<boolean> {
    return this.isLogged(this.router.url.includes('/admin/'), state.url.replace('/', ''));
  }

  isLogged(shouldBeAdmin: boolean, comingFrom?: string): Observable<boolean> {
    return new Observable(observer => {
      return this.http.get(environment.backUrl + 'user/').subscribe(
        (result: any) => {
          console.log('Result of get user : ', result);
          let isCorrectlyLoggedIn = true;
          if (shouldBeAdmin) {
            isCorrectlyLoggedIn = result.user.isAdmin;
          }
          observer.next(isCorrectlyLoggedIn);
        },
        (error: any) => {
          console.warn('Get user has failed : ', error);
          let path = ['/login'];
          if (comingFrom !== null) {
            path = [...path, 'comingFrom', comingFrom];
          }
          console.log('Navigation to : ', path);
          observer.next(true);
          this.router.navigate(path);
        }
      );
    });
  }

  login(login: string, password: string): Observable<boolean> {
    return new Observable(observer => {
      this.http.post(environment.backUrl + 'public/login', {login: login, password: password}).subscribe(
        (success: any) => {
          console.log('Login success : ', success);
          this.user = success;
          observer.next(true);
        },
        error => {
          console.log('Login failed : ', error);
          observer.next(false);
        }
      );
    });
  }

  getToken(): string {
    return (this.user && this.user.token) ? this.user.token : '';
  }
}
