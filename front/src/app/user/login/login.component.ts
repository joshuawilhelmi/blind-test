import { ActivatedRoute, Router } from '@angular/router';
import { OnInit, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login;
  password;
  error;
  private comingFrom: string;

  constructor(private route: ActivatedRoute, private router: Router, private loginService: LoginService) {
    route.paramMap.subscribe((paramsAsMap: any) => {
      console.log('Params : ', paramsAsMap.params);
      this.comingFrom = paramsAsMap.params.comingFrom;
    });
  }

  ngOnInit() {
  }

  submitLogin() {
    this.loginService.login(this.login, this.password).subscribe(
      success => {
        this.router.navigate(['home']);
      },
      error => {
        this.error = error.message;
      }
    );
  }

}
