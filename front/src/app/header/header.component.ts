import { Component, OnInit } from '@angular/core';
import { LoginService } from '../user/login.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title = 'BLIND TEST'.split('');

  constructor(public loginService: LoginService) { }

  ngOnInit() {
  }

}
